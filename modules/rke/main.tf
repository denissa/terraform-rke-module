resource "rke_cluster" "cluster" {

  dynamic nodes {
    for_each = var.master_nodes
    content {
      node_name         = nodes.key
      address           = nodes.value.internal_ip
      internal_address  = nodes.value.internal_ip
      hostname_override = nodes.key
      user              = var.system_user
      role              = ["controlplane", "etcd"]
      labels            = var.master_labels
      dynamic taints {
        for_each = var.master_taints
        content {
          key    = lookup(taints.value, "key")
          value  = lookup(taints.value, "value")
          effect = lookup(taints.value, "effect", "NoSchedule")
        }
      }
    }
  }

  dynamic nodes {
    for_each = var.worker_nodes
    content {
      node_name         = nodes.key
      address           = nodes.value.internal_ip
      internal_address  = nodes.value.internal_ip
      hostname_override = nodes.key
      user              = var.system_user
      role              = ["worker"]
      labels            = var.worker_labels
      dynamic taints {
        for_each = var.worker_taints
        content {
          key    = lookup(taints.value, "key")
          value  = lookup(taints.value, "value")
          effect = lookup(taints.value, "effect", "NoSchedule")
        }
      }
    }
  }

  ssh_agent_auth = var.use_ssh_agent
  ssh_key_path   = var.ssh_key_file

  kubernetes_version = var.kubernetes_version

  dynamic upgrade_strategy {
    for_each = var.upgrade_strategy
    content {
      drain                        = upgrade_strategy.value.drain
      max_unavailable_controlplane = upgrade_strategy.value.max_unavailable_controlplane
      max_unavailable_worker       = upgrade_strategy.value.max_unavailable_worker

      dynamic drain_input {
        for_each = var.drain_options
        content {
          delete_local_data  = drain_input.value.delete_local_data
          force              = drain_input.value.force
          grace_period       = drain_input.value.grace_period
          ignore_daemon_sets = drain_input.value.ignore_daemon_sets
          timeout            = drain_input.value.timeout
        }
      }
    }
  }

  services {
    etcd {
      extra_args = var.etcd_extra_args
      dynamic backup_config {
        for_each = var.etcd_backup_config
        content {
          interval_hours = backup_config.value.interval_hours
          retention      = backup_config.value.retention

          dynamic s3_backup_config {
            for_each = var.s3_backup_config
            content {
              access_key  = s3_backup_config.value.access_key
              secret_key  = s3_backup_config.value.secret_key
              bucket_name = s3_backup_config.value.bucket_name
              region      = s3_backup_config.value.region
              endpoint    = s3_backup_config.value.endpoint
            }
          }
        }
      }
    }

    kube_api {
      extra_args = var.kube_api_extra_args
    }
    kube_controller {
      extra_args = var.kube_controller_extra_args
    }
    scheduler {
      extra_args = var.kube_scheduler_extra_args
    }
    kubelet {
      extra_args = var.kubelet_extra_args
    }
    kubeproxy {
      extra_args = var.kube_proxy_extra_args
    }
  }

  network {
    plugin = var.network_plugin
  }

  ingress {
    provider = var.deploy_nginx
  }

  authentication {
    strategy = "x509"
    sans     = var.kube_api_extra_sans
  }

  addons_include = var.addons_include
}

resource "local_file" "kube_cluster_yaml" {
  count    = var.write_kubeconfig ? 1 : 0
  filename = "${path.root}/kubeconfig"
  content  = rke_cluster.cluster.kube_config_yaml
}

resource "local_file" "rke_cluster_yaml" {
  count    = var.write_rke_cluster_config ? 1 : 0
  filename = "${path.root}/rke_cluster.yml"
  content  = rke_cluster.cluster.rke_cluster_yaml
}
