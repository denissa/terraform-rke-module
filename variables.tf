variable "cluster_name" {
  type        = string
  description = "Name of the cluster"
}

variable "kubernetes_version" {
  type        = string
  description = "Kubernetes version (RKE)"
}

variable "master_nodes" {
  default     = {}
  description = "Kubernetes Master Nodes"
}

variable "worker_nodes" {
  default     = {}
  description = "Kubernetes Worker Nodes"
}

variable "system_user" {
  type        = string
  description = "Default OS image user"
}

variable "ssh_key_file" {
  type        = string
  description = "Local path to SSH key"
}

variable "use_ssh_agent" {
  type    = bool
  default = "true"
}

variable "master_labels" {
  type        = map(string)
  description = "Master labels"
}

variable "worker_labels" {
  type        = map(string)
  description = "Worker labels"
}

variable "master_taints" {
  type        = list(map(string))
  description = "Master taints"
}

variable "worker_taints" {
  default     = []
  type        = list(map(string))
  description = "Worker taints"
}

variable "network_plugin" {
  type        = string
  description = "Network CNI Plugin"
}

variable "deploy_nginx" {
  type        = string
  description = "Deploy Nginx Ingress"
}

variable "upgrade_strategy" {
  default     = []
  description = "Upgrade Strategy k8s Cluster"
}

variable "drain_options" {
  default     = []
  description = "Upgrade Cluster Drain Options"
}

variable "kube_api_extra_sans" {
  type        = list(string)
  default     = []
  description = "Kube-apiserver Extra Sans adresess"
}

variable "kube_api_extra_args" {
  type        = map(string)
  default     = {}
  description = "Kube-apiserver Extra Argumets"
}

variable "kube_controller_extra_args" {
  type        = map(string)
  default     = {}
  description = "Kube-controller Extra Argumets"
}

variable "kubelet_extra_args" {
  type        = map(string)
  default     = {}
  description = "Kubelet Extra Argumets"
}

variable "kube_scheduler_extra_args" {
  type        = map(string)
  default     = {}
  description = "Kube-scheduler Extra Argumets"
}

variable "kube_proxy_extra_args" {
  type        = map(string)
  default     = {}
  description = "Kube-proxy Extra Argumets"
}

variable "etcd_extra_args" {
  type        = map(string)
  default     = {}
  description = "Etcd Extra Argumets"
}

variable "etcd_backup_config" {
  type        = list(map(string))
  default     = []
  description = "Etcd Backup Config"
}

variable "s3_backup_config" {
  type        = list(map(string))
  default     = []
  description = "Etcd S3 Backup Settings"
}

variable "write_kubeconfig" {
  type        = bool
  default     = true
  description = "Write Kubeconfig"
}

variable "write_rke_cluster_config" {
  type        = bool
  default     = true
  description = "Write RKE Cluster Config"
}

variable "addons_include" {
  type        = list(string)
  default     = []
  description = "Addons Include"
}
