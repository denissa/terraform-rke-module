## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| rke | 1.1.3 |

## Providers

| Name | Version |
|------|---------|
| local | n/a |
| null | n/a |
| rke | 1.1.3 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| addons\_include | Addons Include | `list(string)` | `[]` | no |
| cluster\_name | Name of the cluster | `string` | n/a | yes |
| deploy\_nginx | Deploy Nginx Ingress | `string` | n/a | yes |
| drain\_options | Upgrade Cluster Drain Options | `list` | `[]` | no |
| etcd\_backup\_config | Etcd Backup Config | `list(map(string))` | `[]` | no |
| etcd\_extra\_args | Etcd Extra Argumets | `map(string)` | `{}` | no |
| kube\_api\_extra\_args | Kube-apiserver Extra Argumets | `map(string)` | `{}` | no |
| kube\_api\_extra\_sans | Kube-apiserver Extra Sans adresess | `list(string)` | `[]` | no |
| kube\_controller\_extra\_args | Kube-controller Extra Argumets | `map(string)` | `{}` | no |
| kube\_proxy\_extra\_args | Kube-proxy Extra Argumets | `map(string)` | `{}` | no |
| kube\_scheduler\_extra\_args | Kube-scheduler Extra Argumets | `map(string)` | `{}` | no |
| kubelet\_extra\_args | Kubelet Extra Argumets | `map(string)` | `{}` | no |
| kubernetes\_version | Kubernetes version (RKE) | `string` | n/a | yes |
| master\_labels | Master labels | `map(string)` | n/a | yes |
| master\_nodes | Kubernetes Master Nodes | `map` | `{}` | no |
| master\_taints | Master taints | `list(map(string))` | n/a | yes |
| network\_plugin | Network CNI Plugin | `string` | n/a | yes |
| s3\_backup\_config | Etcd S3 Backup Settings | `list(map(string))` | `[]` | no |
| ssh\_key\_file | Local path to SSH key | `string` | n/a | yes |
| system\_user | Default OS image user | `string` | n/a | yes |
| upgrade\_strategy | Upgrade Strategy k8s Cluster | `list` | `[]` | no |
| use\_ssh\_agent | n/a | `bool` | `"true"` | no |
| worker\_labels | Worker labels | `map(string)` | n/a | yes |
| worker\_nodes | Kubernetes Worker Nodes | `map` | `{}` | no |
| worker\_taints | Worker taints | `list(map(string))` | `[]` | no |
| write\_kubeconfig | Write Kubeconfig | `bool` | `true` | no |
| write\_rke\_cluster\_config | Write RKE Cluster Config | `bool` | `true` | no |

## Outputs

| Name | Description |
|------|-------------|
| rke\_cluster | RKE cluster spec |

